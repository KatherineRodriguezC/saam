# Generated by Django 2.1.2 on 2018-11-04 16:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('salud', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tutor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('runTutor', models.CharField(max_length=10)),
                ('nombreTutor', models.CharField(max_length=100)),
                ('apellidoTutor', models.CharField(max_length=100)),
                ('FechaNacimientoTutor', models.DateField()),
                ('telefonoTutor', models.IntegerField()),
                ('parentescoTutor', models.CharField(max_length=10)),
            ],
        ),
    ]
