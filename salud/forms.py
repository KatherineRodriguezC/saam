from django import forms
from .models import Persona
from django.contrib.auth.models import User

class UsuarioForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('username', 'password',)