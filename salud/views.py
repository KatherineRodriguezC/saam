from django.shortcuts import render
from django.http import HttpResponse
from .models import Persona
from .models import Tutor
from .forms import UsuarioForm
from django.shortcuts import redirect
from django.core.mail import send_mail
#importar user
from django.contrib.auth.models import User
#sistema de autenticación 
from django.contrib.auth import authenticate,logout, login as auth_login

from django.contrib.auth.decorators import login_required

# Create your views here.

def index(request):
    return render(request,'index.html',{})

def registro(request):
    return render(request,'registro.html',{})

def login(request):
    return render(request,'login.html',{})

def home(request):
    usuario = request.session.get('usuario',None)
    return render(request,'home.html',{})

def crear(request):
    run = request.POST.get('run','')
    nombre = request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    FechaNacimiento = request.POST.get('FechaNacimiento','')
    telefono = request.POST.get('telefono','')
    email = request.POST.get('correo','')
    contrasenia = request.POST.get('contrasenia','')
    prevision = request.POST.get('prevision','')
    InfoMedica = request.POST.get('InfoMedica','')
    persona = Persona(run=run,nombre=nombre,apellido=apellido,FechaNacimiento=FechaNacimiento,telefono=telefono,correo = email,contrasenia = contrasenia,prevision=prevision,InfoMedica=InfoMedica)
    persona.save()
    return render(request,'tutor.html')

def tutor(request):
    runTutor = request.POST.get('run','')
    nombreTutor = request.POST.get('nombre','')
    apellidoTutor = request.POST.get('apellido','')
    FechaNacimientoTutor = request.POST.get('FechaNacimiento','')
    telefonoTutor = request.POST.get('telefono','')
    parentescoTutor = request.POST.get('parentesco','')
    tutor = Tutor(runTutor=runTutor,nombreTutor=nombreTutor,apellidoTutor=apellidoTutor,FechaNacimientoTutor=FechaNacimientoTutor,telefonoTutor=telefonoTutor,parentescoTutor=parentescoTutor)
    tutor.save()
    return render(request,'usuario.html')

def user(request):
    username = request.POST.get('username','')
    correo = request.POST.get('correo','')
    contrasenia = request.POST.get('contrasenia','')
    user = Usuario(username=username,correo=correo,contrasenia=contrasenia)
    user.save()
    return render(request,'home.html')

def cerrar_session(request):
    del request.session['usuario']
    return redirect('index.html')

def login(request):
    return render(request,'login.html',{})

def login_iniciar(request):
    correo = request.POST.get('correo','')
    contrasenia = request.POST.get('contrasenia','')
    persona = Persona.objects.filter(correo=correo).filter(contrasenia=contrasenia).first()
    if persona is not None:
        request.session['usuario'] = persona[0].nombre
        request.session['id'] = persona[0].id
        return redirect('home')
    else:
        return redirect('home')

    
def email(request):
    send_mail(
    'Mensaje Urgente',
    'Su ... necesita atencion medica, favor de dirigirse a su centro asistencial as cercano o contactar algun centro asistencial más cercano',
    'nicolassone9@gmail.com',
    ['n.jofrej@alumnos.duoc.cl','kat.rodriguezc@alumnos.duoc.cl','ab.jarav@alumnos.duoc.cl'],
    fail_silently=False)

    return render(request,'home.html',{})

