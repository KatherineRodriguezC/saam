from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.urls import reverse

# Create your models here.


class Persona(models.Model):
    run = models.CharField(max_length=10)
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    FechaNacimiento = models.DateField()
    telefono = models.IntegerField()
    correo = models.CharField(max_length=40)
    contrasenia = models.CharField(max_length=40)
    prevision = models.CharField(max_length=10)
    InfoMedica = models.CharField(max_length=1000)


class Tutor(models.Model):
    runTutor = models.CharField(max_length=10)
    nombreTutor = models.CharField(max_length=100)
    apellidoTutor = models.CharField(max_length=100)
    FechaNacimientoTutor = models.DateField()
    telefonoTutor = models.IntegerField()
    parentescoTutor = models.CharField(max_length=10)


class UserManager(BaseUserManager):
    def create_user(self, email, password=None):

        if not email:
            raise ValueError('Usuario debe tener un correo electronico')

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user


# class user(AbstractBaseUser):

#     objects = UserManager()

#     email = models.EmailField(
#         verbose_name='email adress',
#         aux_length=255,
#         unique=true,
#     )

#     USERNAME_FIELD = 'email'
#     REQUIRED_FIELDS = []
