from django.urls import path
from . import views
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('',views.index, name="index"),
    path('formulario',views.registro, name="registro"),
    path('login/',views.login, name="login"),
    path('persona',views.crear, name="crear"),
    path('tutor',views.tutor, name="tutor"),
    path('home',views.home, name="home"),
    path('cerrarsession',views.cerrar_session,name="cerrar_session"),
    path('login/iniciar',views.login_iniciar,name="iniciar"),
    path('user',views.home, name="home"),
    path('email', views.email, name="email")
]