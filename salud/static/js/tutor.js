$.validator.addMethod('daterange', function (value, element) {
    if (this.optional(element)) {
        return true;
    }

    var startDate = Date.parse('1900-1-1'),
        endDate = Date.parse('2000-1-1'),
        enteredDate = Date.parse(value);

    if (isNaN(enteredDate)) return false;

    return ((startDate <= enteredDate) && (enteredDate <= endDate));
}, "");

$("#tutor").validate({
    rules: {
        nombre: {
            required: true,
            lettersonly: true
        },
        apellido: {
            required: true,
            lettersonly: true
        },
        run: {
            required: true,
        },
        FechaNacimiento: {
            required: true,
            daterange: true
        },
        telefono: {
            required: true,
            integer: true
        },
        parentesco: {
            required: true
        }
    },
    messages: {
        nombre: {
            required: 'Este campo es obligatorio',
            lettersonly: 'Debe ingresar solo letras'
        },
        apellido: {
            required: 'Este campo es obligatorio',
            lettersonly: 'Debe ingresar solo letras'
        },
        run: {
            required: 'Este campo es obligatorio',
        },
        FechaNacimiento: {
            required: 'Este campo es obligatorio',
            daterange: 'Debes ser mayor de edad'
        },
        telefono: {
            required: 'Este campo es obligatorio',
            integer: 'Ingrese solo numeros'
        },
        parentesco: {
            required: 'Este campo es obligatorio'
        }
    }
})