$("#registro").submit(function () {
    var resultado = $(this).serializeObject()
    arreglo.push(resultado)
    $("#elementos").html("")
    arreglo.forEach(elementos => {
        $("#elementos").append('<div class="col-md-2">' +
            '<img src="' + elementos.url + '"' +
            'class="img-fluid" alt="imagen" srcset="">' +
            '<h5>' + elementos.titulo + '</h5>' +
            '<p>' + elementos.pais + '</p>' +
            '</div>')
    });
})


$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(function () {

    $.validator.setDefaults({
        errorClass: 'help-block',
        highlight: function (element) {
            $(element)
                .closest('.form-group')
                .addClass('has-error');
        },
        unhighlight: function (element) {
            $(element)
                .closest('.form-group')
                .removeClass('has-error');
        }
    });

    $.validator.addMethod('daterange', function (value, element) {
        if (this.optional(element)) {
            return true;
        }

        var startDate = Date.parse('1900-1-1'),
            endDate = Date.parse('1953-1-1'),
            enteredDate = Date.parse(value);

        if (isNaN(enteredDate)) return false;

        return ((startDate <= enteredDate) && (enteredDate <= endDate));
    }, "");


 


    $("#registro").validate({
        rules: {
            nombre: {
                required: true,
                lettersonly: true
            },
            apellido: {
                required: true,
                lettersonly: true
            },
            run: {
                required: true,
            },
            FechaNacimiento: {
                required: true,
                daterange: true
            },
            telefono: {
                required: true,
                integer: true
            },
            prevision: {
                required: true
            },
            infoMedica: {
                required: true
            }
        },
        messages: {
            nombre: {
                required: 'Este campo es obligatorio',
                lettersonly: 'Debe ingresar solo letras'
            },
            apellido: {
                required: 'Este campo es obligatorio',
                lettersonly: 'Debe ingresar solo letras'
            },
            run: {
                required: 'Este campo es obligatorio',
            },
            FechaNacimiento: {
                required: 'Este campo es obligatorio',
                daterange: 'Debes ser mayor de edad'
            },
            telefono: {
                required: 'Este campo es obligatorio',
                integer: 'Ingrese solo numeros'
            },
            prevision: {
                required: 'Este campo es obligatorio'
            },
            infoMedica: {
                required: 'Este campo es obligatorio'
            }
        }
    })
})

$("#log").validate({
    rules: {
        username: {
            required: true
            
        },
        password: {
            required:true
        }
    },
    messages: {
        username: {
            required: 'Este campo es obligatorio'
        },
        password: {
            required: 'Este campo es obligatorio'
        }
    }
})
